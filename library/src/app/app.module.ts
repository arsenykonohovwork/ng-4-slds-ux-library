import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentsModule } from './components/components.module';
import { AttributeDirectivesModule } from './attribute-directives/attribute-directives.module';
import { AssetsModule } from './assets/assets.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ComponentsModule,
    AttributeDirectivesModule,
    AssetsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}








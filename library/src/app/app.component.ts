import { Component } from '@angular/core';

@Component({
  selector: 'jsng-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'jsng';
}

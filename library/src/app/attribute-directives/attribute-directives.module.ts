import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttributeDirectivesRoutingModule } from './attribute-directives-routing.module';
import { AttributeDirectivesComponent } from './attribute-directives.component';

import { CellCursorPreviewComponent } from './cell-cursor-preview/cell-cursor-preview.component';
import { CellCursorDirective } from './cell-cursor/cell-cursor.directive';
import { TableModule } from '../components/table/table.module';


@NgModule({
  imports: [
    CommonModule,
    AttributeDirectivesRoutingModule,
    TableModule,
  ],
  declarations: [
    AttributeDirectivesComponent,
    CellCursorPreviewComponent,
    CellCursorDirective,
  ],
  exports: [
    AttributeDirectivesComponent,
    CellCursorPreviewComponent,
    CellCursorDirective,
  ]
})
export class AttributeDirectivesModule {}


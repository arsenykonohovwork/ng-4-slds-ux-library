import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AttributeDirectivesComponent } from './attribute-directives.component';
import { CellCursorPreviewComponent } from './cell-cursor-preview/cell-cursor-preview.component';


const routes: Routes = [
  {
    path:'',
    component:AttributeDirectivesComponent,
    children: [
      {path:'cell-cursor', component:CellCursorPreviewComponent},
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AttributeDirectivesRoutingModule { }










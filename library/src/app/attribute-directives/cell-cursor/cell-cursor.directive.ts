import { 
  Directive, 
  OnInit,
  ElementRef, HostListener,
  Input, Output, EventEmitter, 
  SimpleChange
} from '@angular/core';


@Directive({
  selector: '[jsngCellCursor]',
  host: {'class': 'jsng-cell-cursor-root'}
})
export class CellCursorDirective implements OnInit {
  private _timer:any = null;
  private _downElement:any = null;
  private _upElement:any = null;
  private _elementsSchema:any[] = [];
  private _markerStartPos:any = null;
  private _axisFix:any = null;
  private _cursorElement:any = null;
  @Input('jsngCellCursor') jsngCellCursorOptions:any = {}
  @Input() jsngcellCursorTableData:any = [];
  @Output() jsngCellCursorCalculate = new EventEmitter();
  constructor(
    private _ElementRef:ElementRef,
  ) {}
  // --------------------------------------------------
  // LIFECYCLE HOOKS:
  ngOnInit() {}
  ngAfterViewInit() {}
  ngOnChanges(changes:SimpleChange) {
    if (changes['jsngcellCursorTableData'].currentValue.length) {
      setTimeout(()=>{ this._initEnvironment(); }, 10);
    }
  }
  // --------------------------------------------------
  // DOM EVENT HANDLING:
  @HostListener('document:mousedown', ['$event']) onMousedown(event) {
    if (this.jsngcellCursorTableData.length) {
      let closestCell      = event.target.closest('.'+this.jsngCellCursorOptions.elements.cell);
      let closestContainer = event.target.closest('.'+this.jsngCellCursorOptions.elements.container);
      let container        = document.querySelector('.'+this.jsngCellCursorOptions.elements.container);
      let containerRect    = container.getBoundingClientRect();
      if (closestCell && closestContainer) {
        this._downElement    = closestCell;
        this._markerStartPos = { x:Math.floor(event.clientX-containerRect.left), y:Math.floor(event.clientY-containerRect.top) };
        this._cursorElement  = this._setCursorElement(container, this._markerStartPos); // draw cursor element;
      }
    }
  }
  @HostListener('document:mouseup', ['$event']) onMouseup(event) {
    if (this.jsngcellCursorTableData.length) {
      this._upElement      = event.target.closest('.'+this.jsngCellCursorOptions.elements.cell);
      let closestContainer = event.target.closest('.'+this.jsngCellCursorOptions.elements.container);
      let container        = document.querySelector('.'+this.jsngCellCursorOptions.elements.container);
      let containerRect    = container.getBoundingClientRect();
      if (closestContainer && this._downElement) {
        this._pushOutput(this._calculateActivePositions(this._elementsSchema, this._downElement, this._upElement));
      } else if (this._downElement) {
        // console.log('UP OUTSIDE');
        // let outsidePos = this._obtainOutsidePositions(containerRect, {x:event.clientX, y:event.clientY});
        // let perimeterElems = ???
        // let upElement = ???
        // this._pushOutput(this._calculateActivePositions(this._elementsSchema, this._downElement, this._upElement));
      }
      // reset ------------------------
      this._cursorElement  ? this._cursorElement.remove() : null;
      this._markerStartPos = null;
      this._downElement    = null;
      this._upElement      = null;
    }
  }
  @HostListener('document:mousemove', ['$event']) onMouseMove(event) {
    if (this.jsngcellCursorTableData.length) {
      clearTimeout(this._timer);
      this._timer = setTimeout(() => {
        let container        = document.querySelector('.'+this.jsngCellCursorOptions.elements.container);
        let closestContainer = event.target.closest('.'+this.jsngCellCursorOptions.elements.container);
        let containerRect    = container.getBoundingClientRect();
        let cursorPos        = {x:Math.floor(event.clientX-containerRect.left), y:Math.floor(event.clientY-containerRect.top)};
        if (closestContainer && this._downElement) {
          this._drawCursor(this._cursorElement, this._markerStartPos, cursorPos);
        } else if (this._downElement) {
          // console.log('MOVE OUTSIDE');
          // let outsidePos = this._obtainOutsidePositions(containerRect, {x:event.clientX, y:event.clientY});
          // this._drawCursor(this._cursorElement, this._markerStartPos, outsidePos);
        }
      }, 5);
    }
  }
  // --------------------------------------------------
  // PRIVATE:
  private _initEnvironment() {
    let root             = this._ElementRef.nativeElement;
    let container        = root.querySelector('.'+this.jsngCellCursorOptions.elements.container);
    let cellParents      = container.querySelectorAll('.'+this.jsngCellCursorOptions.elements.cellParent);
    let cells            = Array.from(container.querySelectorAll('.'+this.jsngCellCursorOptions.elements.cell));
    this._elementsSchema = this._retrieveElementsShema(cellParents); // schema for selecting active elements;
    this._axisFix = {
      top:    Math.abs(Math.floor(container.getBoundingClientRect().top    - root.getBoundingClientRect().top)),
      bottom: Math.abs(Math.floor(container.getBoundingClientRect().bottom - root.getBoundingClientRect().bottom)),
      left:   Math.abs(Math.floor(container.getBoundingClientRect().left   - root.getBoundingClientRect().left)),
      right:  Math.abs(Math.floor(container.getBoundingClientRect().right  - root.getBoundingClientRect().right)),
    };
    container.classList.add("jsng-cell-cursor-container");
    root.style.position = 'relative';
    cells.forEach(v=>v['classList'].add('jsng-cell-cursor-cell'));
  }
  private _retrieveElementsShema(cellParents) {
    // create DOM schema relevant to input data;
    return Array.from(cellParents).filter(v=>v['nodeType']===1).map((p,pId)=>({
      parent:p,
      id:pId,
      cells: Array.from(p['childNodes']).filter((x,xid)=>x['nodeType']===1).map((c,cId)=>({
        cell:c,
        id:cId
      })),
    }));
  }
  private _setCursorElement(container, position) {
    let _x = this._axisFix.left;
    let _y = this._axisFix.top;
    let elem = document.createElement('div');
    let child = ['<div class="jsng-cell-cursor-startpoint" style="', 'background:rgba(0,0,0,0.05);', 'border:dashed 2px #777;', 'position:absolute;', 'right:0px;', 'left:0px;', 'height:0px;', 'width:0px;', '"><div>',].join('');
    // -------------------------
    elem.style.display  = 'none';
    elem.style.position = 'absolute';
    elem.style.left     = position.x+_x+'px';
    elem.style.top      = position.y+_y+'px';
    elem.style.width    = '0px';
    elem.style.height   = '0px';
    elem.style.zIndex   = '1000';
    // -------------------------
    elem.innerHTML = child;
    elem.className = 'jsng-cell-cursor-marker';
    // -------------------------
    container.appendChild(elem);
    return elem;
  }
  private _drawCursor(elem, start, dyn) {
    let sift  = 3;
    let child = Array.from(elem.childNodes).filter((x,xid)=>x['nodeType']===1)[0];
    // --------------------------------
    let _x = dyn.x-start.x-sift;
    let _y = dyn.y-start.y-sift;
    // --------------------------------
    let top  = 0;
    let left = 0;
    // --------------------------------
    if (Math.abs(_x)>10 || Math.abs(_y)>10) {
      switch(true) {
        case(_x<=0 && _y<=0): left=_x+sift+5; top=_y+sift+5; break;
        case(_x>=0 && _y<=0): top=_y+sift; break;
        case(_x>=0 && _y>=0): break;
        case(_x<=0 && _y>=0): left=_x+sift; break;
        default: break;
      }
      elem.style.display        = 'block';
      child['style'].width      = Math.abs(_x) +'px';
      child['style'].height     = Math.abs(_y) +'px';
      child['style'].marginTop  = top +'px';
      child['style'].marginLeft = left +'px';
    } else {
      elem.style.display = 'none';
    }
  }
  private _calculateActivePositions(shema, downElelm, upElem) {
    let down,up = {xxx:0,yyy:0}; // UP/DOWN coordinates;
    let leftTop,rghtBtm = {x:0,y:0};
    // Obtain UP/DOWN coordinates and set id-s for parents and cells;
    let collection = shema.map((p, pId)=>{
      return {
        id:pId,
        cells:p.cells.map((c,cId)=>{
          down = (c.cell===downElelm) ? {xxx:pId, yyy:cId} : down;
          up   = (c.cell===upElem)    ? {xxx:pId, yyy:cId} : up;
          return {id:cId};
        }),
      }
    });
    // convert UP/DOWN to edge positions (lett/top && right/bottom);
    leftTop = {x:(up.xxx<=down.xxx)?up.xxx:down.xxx, y:(up.yyy<=down.yyy)?up.yyy:down.yyy};
    rghtBtm = {x:(up.xxx>=down.xxx)?up.xxx:down.xxx, y:(up.yyy>=down.yyy)?up.yyy:down.yyy}
    // set active positions and get rid of DOM elements;
    collection.map((p,pId)=>{
      p.cells.map((c,cId)=>{
        c.active = (pId>=leftTop.x && pId<=rghtBtm.x) && (cId>=leftTop.y && cId<=rghtBtm.y);
        return c;
      });
      return p;
    });
    return collection;
  }
  private _pushOutput(output) {
    this.jsngCellCursorCalculate.emit(output);
  }
  // --------------------------
  // in progress:
  private _obtainOutsidePositions(containerRect, eventPos) {
    let rect = {
      top:    Math.floor(containerRect.top),
      bottom: Math.floor(containerRect.bottom),
      left:   Math.floor(containerRect.left),
      right:  Math.floor(containerRect.right - containerRect.width),
    };
    console.log(rect);
    // console.log(eventPos);
  }
}


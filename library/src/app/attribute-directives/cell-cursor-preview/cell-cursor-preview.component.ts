import { Component, OnInit } from '@angular/core';


const CELL_CURSOR_OPTIONS_TABLE = {
  elements: {
    container:  'js-pseudo-table__body',
    cellParent: 'js-pseudo-table__row',
    cell:       'js-pseudo-table__col',
  }
};
const CELL_CURSOR_OPTIONS_ALTERNATIVE = {
  elements: {
    container:  'js-data-table__body',
    cellParent: 'js-data-table__col',
    cell:       'js-data-table__row',
  }
};


@Component({
  selector: 'jsng-cell-cursor-preview',
  templateUrl: './cell-cursor-preview.component.html',
  styleUrls: ['./cell-cursor-preview.component.scss']
})
export class CellCursorPreviewComponent implements OnInit {
  mergingType:string = 'XOR';
  // ------------------------------------------------
  // Example for TABLE:
  cellCursorOptionsForTable:any = {...CELL_CURSOR_OPTIONS_TABLE};
  collectionHead:any[] = [];
  collectionBody:any[] = [];
  collectionFoot:any[] = [];
  // ------------------------------------------------
  // Example for DATA-TABLE:
  cellCursorOptionsForAlternativeTable:any = {...CELL_CURSOR_OPTIONS_ALTERNATIVE};
  week:any[]       = [];
  weekHead:any[][] = [];
  weekBody:any[][] = [];
  weekFoot:any[][] = [];
  // ------------------------------------------------
  constructor() {}
  ngOnInit() {
    setTimeout(()=>{
      this.getData();
    }, 3000);
  }
  getData() {
    // ---------------------------------------------------------------
    this.week     = ['Mon','Tue','Wed','Thr','Fri','Sat','Sun'].map(x=>Array.from({length:6},(v,i)=>({title:x, hour:i, active:false, status:'avlbl'})) );;
    this.weekHead = this.week.map(v=>[v[0]]);
    this.weekBody = this.week;
    this.weekFoot = this.week.map(v=>[v[0]]);
    // ---------------------------------------------------------------
    this.collectionHead = [
      [{active:false,label:'Head'}, {active:false,label:'Head'}, {active:false,label:'Head'}, {active:false,label:'Head'}, {active:false,label:'Head'}, {active:false,label:'Head'}],
    ];
    this.collectionBody = [
      [{active:false,label:'0.0'}, {active:false,label:'0.1'}, {active:false,label:'0.2'}, {active:false,label:'0.3'}, {active:false,label:'0.4'}, {active:false,label:'0.5'}],
      [{active:false,label:'1.0'}, {active:false,label:'1.1'}, {active:false,label:'1.2'}, {active:false,label:'1.3'}, {active:false,label:'1.4'}, {active:false,label:'1.5'}],
      [{active:false,label:'2.0'}, {active:false,label:'2.1'}, {active:false,label:'2.2'}, {active:false,label:'2.3'}, {active:false,label:'2.4'}, {active:false,label:'2.5'}],
      [{active:false,label:'3.0'}, {active:false,label:'3.1'}, {active:false,label:'3.2'}, {active:false,label:'3.3'}, {active:false,label:'3.4'}, {active:false,label:'3.5'}],
      [{active:false,label:'4.0'}, {active:false,label:'4.1'}, {active:false,label:'4.2'}, {active:false,label:'4.3'}, {active:false,label:'4.4'}, {active:false,label:'4.5'}],
      [{active:false,label:'5.0'}, {active:false,label:'5.1'}, {active:false,label:'5.2'}, {active:false,label:'5.3'}, {active:false,label:'5.4'}, {active:false,label:'5.5'}],
      [{active:false,label:'6.0'}, {active:false,label:'6.1'}, {active:false,label:'6.2'}, {active:false,label:'6.3'}, {active:false,label:'6.4'}, {active:false,label:'6.5'}],
    ];
    this.collectionFoot = [
      [{active:false,label:'Foot'}, {active:false,label:'Foot'}, {active:false,label:'Foot'}, {active:false,label:'Foot'}, {active:false,label:'Foot'}, {active:false,label:'Foot'}],
    ];
  
  }
  onCursorCalculateTable(event) {
    // event -> collection relevant to cellCursor input data (collectionBody);
    this.collectionBody.map((r,rId)=>{
      return r.map((c,cId)=>{
        // ------------------------
        switch(this.mergingType) {
          case'XOR': c.active = (c.active === event[rId].cells[cId].active) ? false : true; break;
          case'AND': c.active = (c.active) ? true : event[rId].cells[cId].active; break;
          case'SINGLE_SELECT': c.active = event[rId].cells[cId].active; break;
          default: break;
        }
        return c;
      });
    });
  }
  onCursorCalculateAlternativeTable(event) {
    // event -> collection relevant to cellCursor input data (weekBody);
    this.weekBody.map((col,colId)=>{
      return col.map((cell,cellId)=>{
        switch(this.mergingType) {
          case'XOR': cell.active = (cell.active === event[colId].cells[cellId].active) ? false : true; break;
          case'AND': cell.active = (cell.active) ? true : event[colId].cells[cellId].active; break;
          case'SINGLE_SELECT': cell.active = event[colId].cells[cellId].active; break;
          default: break;
        }
        return cell;
      });
    });
  }
}



import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CellCursorPreviewComponent } from './cell-cursor-preview.component';

describe('CellCursorPreviewComponent', () => {
  let component: CellCursorPreviewComponent;
  let fixture: ComponentFixture<CellCursorPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CellCursorPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CellCursorPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

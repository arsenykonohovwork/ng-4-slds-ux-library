import {
  Component, 
  OnInit, AfterViewInit, 
  Input, 
  ViewChild, ViewChildren, 
  ContentChild, ContentChildren, 
  QueryList, 
  TemplateRef, 
} from '@angular/core';


@Component({
  selector: 'jsng-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, AfterViewInit {
  @Input() captionData:string = '';
  @Input() tableBody:any[][] = [[]];
  @Input() tableHead:any[] = [];
  @Input() tableFoot:any[] = [];
  @ContentChild('headCellTemplate') headCellRef: TemplateRef<any>;
  @ContentChild('bodyCellTemplate') bodyCellRef: TemplateRef<any>;
  @ContentChild('footCellTemplate') footCellRef: TemplateRef<any>;
  // ----------------------------------------------
  constructor() {}
  ngAfterViewInit() {}
  ngOnInit() {}
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table/table.component';
import { PseudoTableComponent } from './pseudo-table/pseudo-table.component';
import { DataTableComponent } from './data-table/data-table.component';



@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TableComponent,
    PseudoTableComponent,
    DataTableComponent,
  ],
  exports: [
    TableComponent,
    PseudoTableComponent,
    DataTableComponent,
  ]
})
export class TableModule {}

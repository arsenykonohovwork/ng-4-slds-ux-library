import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PseudoTableComponent } from './pseudo-table.component';

describe('PseudoTableComponent', () => {
  let component: PseudoTableComponent;
  let fixture: ComponentFixture<PseudoTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PseudoTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PseudoTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

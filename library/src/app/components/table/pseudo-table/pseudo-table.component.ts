import {
  Component, 
  OnInit, AfterViewInit, 
  Input, 
  ViewChild, ViewChildren, 
  ContentChild, ContentChildren, 
  QueryList, 
  TemplateRef, 
} from '@angular/core';


@Component({
  selector: 'jsng-pseudo-table',
  templateUrl: './pseudo-table.component.html',
  styleUrls: ['./pseudo-table.component.scss']
})
export class PseudoTableComponent implements OnInit {
  @Input() captionData:string = '';
  @Input() tableBody:any[][] = [[]];
  @Input() tableHead:any[] = [];
  @Input() tableFoot:any[] = [];
  @ContentChild('headCellTemplate') headCellRef: TemplateRef<any>;
  @ContentChild('bodyCellTemplate') bodyCellRef: TemplateRef<any>;
  @ContentChild('footCellTemplate') footCellRef: TemplateRef<any>;
  // ----------------------------------------------
  constructor() {}
  ngOnInit() {}
}



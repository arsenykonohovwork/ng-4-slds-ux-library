import {
  Component, 
  OnInit, AfterViewInit, 
  Input, 
  ViewChild, ViewChildren, 
  ContentChild, ContentChildren, 
  QueryList, 
  TemplateRef,
  SimpleChange, 
} from '@angular/core';


@Component({
  selector: 'jsng-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {
  @Input() captionData:string = '';
  @Input() tableBody:any[][] = [[]];
  @Input() tableHead:any[] = [];
  @Input() tableFoot:any[] = [];
  @ContentChild('headCellTemplate') headCellRef:TemplateRef<any>;
  @ContentChild('bodyCellTemplate') bodyCellRef:TemplateRef<any>;
  @ContentChild('footCellTemplate') footCellRef:TemplateRef<any>;
  // ----------------------------------------------
  colWidth:string;
  constructor() {}
  ngOnInit() {
    // this.colWidth = (100/this.tableBody.length).toFixed(2);
  }
  ngOnChanges(changes:SimpleChange) {
    if (changes['tableBody'].currentValue.length) {
      setTimeout(()=>{
        this.colWidth = (100/this.tableBody.length).toFixed(2);
      },100);
    }
  }
}









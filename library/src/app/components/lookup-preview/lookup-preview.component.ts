import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jsng-lookup-preview',
  templateUrl: './lookup-preview.component.html',
  styleUrls: ['./lookup-preview.component.scss']
})
export class LookupPreviewComponent implements OnInit {
  contacts:any[] = [];
  contact:any = null;
  // ------------------------------
  constructor() {}
  ngOnInit() {}
  // ------------------------------
  onClearContact() {
    this.contact = null;
  }
  onChooseFromLookup(item) {
    console.log('ITEM IS CHOOSEN:', item);
  }
  obtainContacts(input) {
    console.log('VFREMOTE FOR COLLECTION (after input):', input);
    if (input.length) {
      Promise.resolve([
        {label:'Iwtem', phoneNumber:19032932084230},
        {label:'Item', phoneNumber:20034234000030},
        {label:'Item', phoneNumber:20034234000030},
        {label:'Item', phoneNumber:39099999999230},
        {label:'Item', phoneNumber:20034234000030},
        {label:'Item', phoneNumber:20034234000030},
        {label:'Item', phoneNumber:20034234000030},
        {label:'Item', phoneNumber:20034234000030},
        {label:'Item', phoneNumber:20034234000030},
        {label:'Item', phoneNumber:49037777777230},
        {label:'Item', phoneNumber:20034234000030},
        {label:'Item', phoneNumber:20034234000030},
        {label:'Item', phoneNumber:59111111114230}
      ]).then(res=>{
        this.contacts = res;
      });
    }
  }

}

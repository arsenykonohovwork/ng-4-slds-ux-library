import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jsng-table-preview',
  templateUrl: './table-preview.component.html',
  styleUrls: ['./table-preview.component.scss']
})
export class TablePreviewComponent implements OnInit {
  // ------------------------------------------------
  // Example for TABLE and PSEUDO-TABLE:
  collectionHead:any[] = [
    [{label:'Head'},{label:'Head'},{label:'Head'}],
  ];
  collectionBody:any[] = [
    [{label:'1.1'},{label:'1.2'},{label:'1.3'}],
    [{label:'2.1'},{label:'2.2'},{label:'2.3'}],
    [{label:'3.1'},{label:'3.2'},{label:'3.3'}],
  ];
  collectionFoot:any[] = [
    [{label:'Foot'},{label:'Foot'},{label:'Foot'}],
  ];
  // ------------------------------------------------
  // Example for DATA-TABLE:
  week:any[]  = ['Mon','Tue','Wed','Thr','Fri','Sat','Sun'].map(x=>Array.from({length:24},(v,i)=>({title:x, hour:i, status:'available'})) );;
  weekHead:any[][] = this.week.map(v=>[v[0]]);
  weekBody:any[][] = this.week;
  weekFoot:any[][] = this.week.map(v=>[v[0]]);
  // ------------------------------------------------
  constructor() {}
  ngOnInit() {
    // console.log(this.week);
  }
}









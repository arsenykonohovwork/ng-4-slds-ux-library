import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsRoutingModule } from './components-routing.module';
import { ComponentsComponent } from './components.component';
// ---------------------------------------------
import { TableModule } from './table/table.module';
import { LookupModule } from './lookup/lookup.module';
import { PicklistModule } from './picklist/picklist.module';
import { ControlsModule } from './controls/controls.module';
// ---------------------------------------------
import { TablePreviewComponent } from './table-preview/table-preview.component';
import { LookupPreviewComponent } from './lookup-preview/lookup-preview.component';
import { PicklistPreviewComponent } from './picklist-preview/picklist-preview.component';
import { ControlsPreviewComponent } from './controls-preview/controls-preview.component';


@NgModule({
  imports: [
    CommonModule,
    ComponentsRoutingModule,
    LookupModule,
    TableModule,
    PicklistModule,
    ControlsModule,
  ],
  declarations: [
    ComponentsComponent,
    TablePreviewComponent,
    LookupPreviewComponent,
    PicklistPreviewComponent,
    ControlsPreviewComponent,
  ],
  exports: [
    ComponentsComponent,
    TablePreviewComponent,
    LookupPreviewComponent,
    PicklistPreviewComponent,
    ControlsPreviewComponent,
  ],
})
export class ComponentsModule { }









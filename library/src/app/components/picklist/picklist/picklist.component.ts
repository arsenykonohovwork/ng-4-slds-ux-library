import {
  Component, 
  OnInit, 
  Input, Output, EventEmitter, 
  ContentChild, 
  TemplateRef
} from '@angular/core';


const OPTIONS = {multiple:false};

@Component({
  selector: 'jsng-picklist',
  templateUrl: './picklist.component.html',
  styleUrls: ['./picklist.component.scss']
})
export class PicklistComponent implements OnInit {
  // --------------------------------------
  collectionValue:any[] = [];
  itemsValue:any[] = [];
  isBodyShown:boolean = false;
  // --------------------------------------
  @Input() options:any = {...OPTIONS};
  @Output() select = new EventEmitter();
  @ContentChild('picklistItemTemplate') picklistItemRef: TemplateRef<any>;
  @Input() get collection() {
    this.itemsValue = [];
    return this.collectionValue.map((v)=>v.data);
  };
  @Output() collectionChange = new EventEmitter();
  set collection(val) {
    this.collectionValue = val.map((v,i)=>({data:v,key:i,selected:false}));
    this.collectionChange.emit(val);
  };
  // --------------------------------------
  constructor() {}
  ngOnInit() {}
  // --------------------------------------
  onToggleDropdown(isShown) {
    this.isBodyShown = !isShown;
  }
  onToggleHeadItems(headItem) {
    // console.log('TOGGLE ITEMS IN HEAD:', headItem);
    this.collectionValue = this.collectionValue.map(v=>Object.assign(v,{selected:(v.key===headItem.key)?!v.selected:v.selected}));
    this.itemsValue = this.collectionValue.filter(v=>v.selected);
    this.select.emit(this.itemsValue.map(v=>v.data))
  }
  onSelect(listItem) {
    if (this.options.multiple) {
      this.collectionValue = this.collectionValue.map(v=>Object.assign(v,{selected:(v.key===listItem.key)?!v.selected:v.selected}));
    } else {
      this.collectionValue = this.collectionValue.map(v=>Object.assign(v,{selected:(v.key===listItem.key)?!v.selected:false}));
    }
    this.itemsValue = this.collectionValue.filter(v=>v.selected);
    this.select.emit(this.itemsValue.map(v=>v.data))
  }
  onClear() {
    this.itemsValue = [];
    this.collectionValue.map(v=>Object.assign(v,{selected:false}));
  }
}



// this.collectionValue = this.collectionValue.map(v=>{
//   v.selected = (v.key === listItem.key) ? !v.selected : false;
//   return v;
// });





// @Output() items = new EventEmitter();
// @Input() get items() {
//   return this.itemsValue.map((v)=>v.data);
// };
// @Output() itemsChange = new EventEmitter();
// set items(val) {
//   this.itemsValue = val.map((v,i)=>({data:v,key:i}));
//   this.itemsChange.emit(val);
// };

// this.items = (this.options.multiple) ? [...this.items, ...items]: [...items];
// this.items = [];
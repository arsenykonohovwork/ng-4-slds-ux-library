import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// -----------------------------------------------------
import { ComponentsComponent } from './components.component';
import { TablePreviewComponent } from './table-preview/table-preview.component';
import { LookupPreviewComponent } from './lookup-preview/lookup-preview.component';
import { PicklistPreviewComponent } from './picklist-preview/picklist-preview.component';
import { ControlsPreviewComponent } from './controls-preview/controls-preview.component';


const routes: Routes = [
  {
    path:'',
    component:ComponentsComponent,
    children: [
      {path:'table',    component:TablePreviewComponent},
      {path:'lookup',   component:LookupPreviewComponent},
      {path:'picklist', component:PicklistPreviewComponent},
      {path:'controls', component:ControlsPreviewComponent},
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule {}



import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jsng-picklist-preview',
  templateUrl: './picklist-preview.component.html',
  styleUrls: ['./picklist-preview.component.scss']
})
export class PicklistPreviewComponent implements OnInit {
  // --------------------------------
  participants:any[] = [];
  selectedParticipants:any = [];
  picklistOptions:any = {
    multiple:true
  };
  // --------------------------------
  constructor() {}
  ngOnInit() {
    this.participants = [
      {label:'Mr. First',   age:37, position:'CEO'},
      {label:'Mr. Second',  age:45, position:'CTO'},
      {label:'Mrs. Third',  age:37, position:'AM'},
      {label:'Mr. Fourth',  age:33, position:'PM'},
      {label:'Mr. Fifth',   age:21, position:'QA'},
      {label:'Mrs. Sixth',  age:25, position:'Developer'},
      {label:'Mr. Seventh', age:30, position:'Developer'},
    ];
  }
  onSelectPicklistItem(event) {
    console.log('SELECT PICKLIST ITEM:', event);
  }
}

import { Component, OnInit, Input, Output, EventEmitter, TemplateRef, ContentChild } from '@angular/core';


const OPTIONS = {
  direction: 'row',
  buttonReduce: false,
  buttonIncrease: false
};


@Component({
  selector: 'jsng-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.scss']
})
export class ControlComponent implements OnInit {
  private _options:any = {};
  private _data:any = {};
  // ----------------------------------------------
  @Input() get options() {return {...this._options}; };
  @Output() optionsChange = new EventEmitter();
  set options(val) {
    let defaultOptions = {...OPTIONS};
    this._options = Object.assign({}, defaultOptions, val);
    this.optionsChange.emit({...this._options});
  };
  // ----------------------------------------------
  @Input() get data() {return {...this._data}; };
  @Output() dataChange = new EventEmitter();
  set data(val) {
    this._data = {...val};
    this.dataChange.emit({...this._data});
  };
  // ----------------------------------------------
  @Output() init     = new EventEmitter();
  @Output() reduce   = new EventEmitter();
  @Output() increase = new EventEmitter();
  @ContentChild('controlReduceBtnTmp')   controlReduceBtnTmpRef:TemplateRef<any>;
  @ContentChild('controlCenterTmp')      controlCenterTmpRef:TemplateRef<any>;
  @ContentChild('controlIncreaseBtnTmp') controlIncreaseBtnTmpRef:TemplateRef<any>;
  // ----------------------------------------------
  constructor() {}
  ngOnInit() {
    setTimeout(()=>{this.init.emit(); },10); // hack
  }
  // ----------------------------------------------
  onReduce(data, options) {
    this.reduce.emit();
  }
  onIncrease(data, options) {
    this.increase.emit();
  }
}







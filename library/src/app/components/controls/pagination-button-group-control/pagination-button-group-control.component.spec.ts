import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginationButtonGroupControlComponent } from './pagination-button-group-control.component';

describe('PaginationButtonGroupControlComponent', () => {
  let component: PaginationButtonGroupControlComponent;
  let fixture: ComponentFixture<PaginationButtonGroupControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginationButtonGroupControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationButtonGroupControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

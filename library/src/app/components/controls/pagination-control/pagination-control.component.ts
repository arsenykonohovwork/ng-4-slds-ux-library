import {Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'jsng-pagination-control',
  templateUrl: './pagination-control.component.html',
  styleUrls: ['./pagination-control.component.scss']
})
export class PaginationControlComponent implements OnInit {
  private _options:any = {};
  private _data:any = {};
  // ----------------------------------------------
  constructor() {}
  ngOnInit() {
    console.log('1 ngOnInit');
  }
  ngAfterContentInit() {
    console.log('2 ngAfterContentInit');
  }
  ngAfterContentChecked() {
    console.log('3 ngAfterContentChecked');
  }
  ngAfterViewInit() {
    console.log('4 ngAfterViewInit');
  }
  ngAfterViewChecked() {
    console.log('5 ngAfterViewChecked');
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationControlComponent } from './pagination-control/pagination-control.component';
import { ControlComponent } from './control/control.component';
import { PathControlComponent } from './path-control/path-control.component';
import { ZoomControlComponent } from './zoom-control/zoom-control.component';
import { PaginationButtonGroupControlComponent } from './pagination-button-group-control/pagination-button-group-control.component';


@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    ControlComponent,
    PaginationControlComponent,
    PathControlComponent,
    ZoomControlComponent,
    PaginationButtonGroupControlComponent,
  ],
  exports: [
    ControlComponent,
    PaginationControlComponent,
    PathControlComponent,
    ZoomControlComponent,
  ]
})
export class ControlsModule {}




import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlsPreviewComponent } from './controls-preview.component';

describe('ControlsPreviewComponent', () => {
  let component: ControlsPreviewComponent;
  let fixture: ComponentFixture<ControlsPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlsPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlsPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'jsng-controls-preview',
  templateUrl: './controls-preview.component.html',
  styleUrls: ['./controls-preview.component.scss']
})
export class ControlsPreviewComponent implements OnInit {
  constructor() {}
  // ==================================================
  // CUSTOM EVENT FROM COMPONENTS:
  ngOnInit() {}
  // ==================================================
  // HELPERS:
  setButtonsState(options, data) {
    options.buttonReduce   = (data.position === 0) ? true : false;
    options.buttonIncrease = (data.position === (data.collection.length-1)) ? true : false;
  }
  // ==================================================
  // simple pagination example features;
  simpleOptions:any = {};
  simpleData:any = {
    collection:['01-11-2011', '02-11-2011', '03-11-2011', '04-11-2011', '05-11-2011', '06-11-2011',],
    position:0,
    value:null,
  };
  simpleInit(payload) {
    this.simpleData.value = this.simpleData.collection[this.simpleData.position];
    this.setButtonsState(this.simpleOptions, this.simpleData);
  }
  simpleReduce(payload) {
    this.simpleData.position = (this.simpleData.position === 0) ? this.simpleData.position : this.simpleData.position-1;
    this.simpleData.value = this.simpleData.collection[this.simpleData.position];
    this.setButtonsState(this.simpleOptions, this.simpleData);
  }
  simpleIncrease(payload) {
    this.simpleData.position = (this.simpleData.position === (this.simpleData.collection.length-1)) ? this.simpleData.position : this.simpleData.position+1;
    this.simpleData.value = this.simpleData.collection[this.simpleData.position];
    this.setButtonsState(this.simpleOptions, this.simpleData);
  }
  // ==================================================
  // pagination with button-group example features:
  bgroupOptions:any = {};
  bgroupData:any = {
    collection:['01-11-2011', '02-11-2011', '03-11-2011', '04-11-2011', '05-11-2011', '06-11-2011',],
    position:2,
    value:null,
  };
  bgroupInit(payload) {
    this.bgroupData.value = this.bgroupData.collection[this.bgroupData.position];
    this.setButtonsState(this.bgroupOptions, this.bgroupData);
  }
  bgroupReduce(payload) {
    this.bgroupData.position = (this.bgroupData.position === 0) ? this.bgroupData.position : this.bgroupData.position-1;
    this.bgroupData.value = this.bgroupData.collection[this.bgroupData.position];
    this.setButtonsState(this.bgroupOptions, this.bgroupData);
  }
  bgroupIncrease(payload) {
    this.bgroupData.position = (this.bgroupData.position === (this.bgroupData.collection.length-1)) ? this.bgroupData.position : this.bgroupData.position+1;
    this.bgroupData.value = this.bgroupData.collection[this.bgroupData.position];
    this.setButtonsState(this.bgroupOptions, this.bgroupData);
  }
  clickBtnGroupLink(id) {
    this.bgroupData.position = id;
    this.bgroupData.value = this.bgroupData.collection[this.bgroupData.position];
    this.setButtonsState(this.bgroupOptions, this.bgroupData);
  }
  // ==================================================
  // pagination as zoom example features:
  zoomOptions:any = {
    direction: 'col'
  };
  zoomData:any = {
    collection:['15', '20', '30', '60', '120', '180'],
    position:4,
    value:null,
  };
  zoomInit(payload) {
    this.zoomData.value = this.zoomData.collection[this.zoomData.position];
    this.setButtonsState(this.zoomOptions, this.zoomData);
  }
  zoomReduce(payload) {
    this.zoomData.position = (this.zoomData.position === 0) ? this.zoomData.position : this.zoomData.position-1;
    this.zoomData.value = this.zoomData.collection[this.zoomData.position];
    this.setButtonsState(this.zoomOptions, this.zoomData);
  }
  zoomIncrease(payload) {
    this.zoomData.position = (this.zoomData.position === (this.zoomData.collection.length-1)) ? this.zoomData.position : this.zoomData.position+1;
    this.zoomData.value = this.zoomData.collection[this.zoomData.position];
    this.setButtonsState(this.zoomOptions, this.zoomData);
  }
}







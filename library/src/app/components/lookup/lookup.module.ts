import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LookupComponent } from './lookup/lookup.component';
import { FormsModule }   from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
  ],
  declarations: [
    LookupComponent,
  ],
  exports: [
    LookupComponent,
  ]
})
export class LookupModule {}











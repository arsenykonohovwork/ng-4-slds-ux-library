import {
  Component, 
  OnInit, 
  Input, Output, EventEmitter, 
  ContentChild, 
  TemplateRef
} from '@angular/core';


const OPTIONS = {
  item: {
    isShow: true
  }
};

@Component({
  selector: 'jsng-lookup',
  templateUrl: './lookup.component.html',
  styleUrls: ['./lookup.component.scss']
})
export class LookupComponent implements OnInit {
  private _collection:any[] = [];
  private _item:any = null;
  private _timer:any;
  inputLookup:string = '';
  @Input() get collection() { return this._collection; }
  @Output() collectionChange = new EventEmitter();
  set collection(val) {
    this._collection = val;
    this.collectionChange.emit(val);
  }
  @Input() get item() { return this._item; }
  @Output() itemChange = new EventEmitter();
  set item(val) {
    this._item = val;
    this.itemChange.emit(this._item);
  }
  @Input() options:any = {...OPTIONS};
  @Output() run = new EventEmitter();
  @Output() choose = new EventEmitter();
  @ContentChild('lookupItemHeadTmp') lookupItemHeadTmpRef: TemplateRef<any>;
  @ContentChild('lookupItemBodyTmp') lookupItemBodyTmpRef: TemplateRef<any>;
  // --------------------------------------
  constructor() {}
  ngOnInit() {}
  // --------------------------------------
  onInput(input) {
    this.collection = [];
    clearTimeout(this._timer);
    this._timer = setTimeout(() => {
      this.run.emit(input);
    }, 500);
  }
  onChoose(item) {
    this.reset();
    this.item = item;
    this.choose.emit(item);
  }
  onClearInput() {
    this.reset();
    this.item = null;
  }
  onStartSearch() {
    this.reset();
    this.item = null;
  }
  // --------------------------------------
  reset() {
    this.collection = [];
    this.inputLookup = '';
  }
}








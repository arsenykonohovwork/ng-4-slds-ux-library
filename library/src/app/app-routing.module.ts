import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ComponentsModule } from './components/components.module';
import { AttributeDirectivesModule } from './attribute-directives/attribute-directives.module';
import { AssetsModule } from './assets/assets.module';



export function loadComponentsMoudle() {
  return ComponentsModule;
};
export function loadAttributeDirectivesMoudle() {
  return AttributeDirectivesModule;
};
export function loadAssetsMoudle() {
  return AssetsModule;
};

const routes: Routes = [
  {path:'', redirectTo:'components', pathMatch:'full'},
  {path:'components',           loadChildren:loadComponentsMoudle},
  {path:'attribute-directives', loadChildren:loadAttributeDirectivesMoudle},
  {path:'assets',               loadChildren:loadAssetsMoudle},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}




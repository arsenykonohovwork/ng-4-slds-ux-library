import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SldsCustomIconComponent } from './slds-custom-icon.component';

describe('SldsCustomIconComponent', () => {
  let component: SldsCustomIconComponent;
  let fixture: ComponentFixture<SldsCustomIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SldsCustomIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SldsCustomIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

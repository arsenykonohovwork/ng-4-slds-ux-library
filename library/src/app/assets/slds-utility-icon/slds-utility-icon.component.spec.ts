import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SldsUtilityIconComponent } from './slds-utility-icon.component';

describe('SldsUtilityIconComponent', () => {
  let component: SldsUtilityIconComponent;
  let fixture: ComponentFixture<SldsUtilityIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SldsUtilityIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SldsUtilityIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

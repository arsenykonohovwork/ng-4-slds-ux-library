import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SldsActionIconComponent } from './slds-action-icon.component';

describe('SldsActionIconComponent', () => {
  let component: SldsActionIconComponent;
  let fixture: ComponentFixture<SldsActionIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SldsActionIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SldsActionIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

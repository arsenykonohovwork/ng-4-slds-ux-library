import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SldsStandardIconComponent } from './slds-standard-icon.component';

describe('SldsStandardIconComponent', () => {
  let component: SldsStandardIconComponent;
  let fixture: ComponentFixture<SldsStandardIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SldsStandardIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SldsStandardIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetsRoutingModule } from './assets-routing.module';
import { AssetsComponent } from './assets.component';
import { SldsActionIconComponent   } from './slds-action-icon/slds-action-icon.component';
import { SldsCustomIconComponent } from './slds-custom-icon/slds-custom-icon.component';
import { SldsStandardIconComponent } from './slds-standard-icon/slds-standard-icon.component';
import { SldsUtilityIconComponent } from './slds-utility-icon/slds-utility-icon.component';


@NgModule({
  imports: [
    CommonModule,
    AssetsRoutingModule
  ],
  declarations: [
    AssetsComponent,
    SldsActionIconComponent,
    SldsCustomIconComponent,
    SldsStandardIconComponent,
    SldsUtilityIconComponent,
  ],
  exports: [
    AssetsComponent,
    SldsActionIconComponent,
  ]
})
export class AssetsModule { }

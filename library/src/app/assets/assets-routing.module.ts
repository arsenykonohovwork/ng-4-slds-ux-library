import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssetsComponent } from './assets.component';

import { SldsActionIconComponent   } from './slds-action-icon/slds-action-icon.component';
import { SldsCustomIconComponent } from './slds-custom-icon/slds-custom-icon.component';
import { SldsStandardIconComponent } from './slds-standard-icon/slds-standard-icon.component';
import { SldsUtilityIconComponent } from './slds-utility-icon/slds-utility-icon.component';


const routes: Routes = [
  {
    path:'',
    component:AssetsComponent,
    children: [
      {path:'slds-action-icon',   component:SldsActionIconComponent},
      {path:'slds-custom-icon',   component:SldsCustomIconComponent},
      {path:'slds-standard-icon', component:SldsStandardIconComponent},
      {path:'slds-utility-icon',  component:SldsUtilityIconComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssetsRoutingModule {}
